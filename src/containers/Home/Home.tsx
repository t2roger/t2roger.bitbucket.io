/*
 * Home.tsx
 * Home container.
 *
 * Created by firstName lastName on date.
 *
 * pwa-starter
 *
 * Copyright © 2009-2019 United States Government as represented by
 * the Program Manager of the DHA: Web and Mobile Technology Program Management Office.
 * All Rights Reserved.
 *
 * Copyright © 2009-2019 Contributors. All Rights Reserved.
 *
 * THIS OPEN SOURCE AGREEMENT ("AGREEMENT") DEFINES THE RIGHTS OF USE,
 * REPRODUCTION, DISTRIBUTION, MODIFICATION AND REDISTRIBUTION OF CERTAIN
 * COMPUTER SOFTWARE ORIGINALLY RELEASED BY THE UNITED STATES GOVERNMENT
 * AS REPRESENTED BY THE GOVERNMENT AGENCY LISTED BELOW ("GOVERNMENT AGENCY").
 * THE UNITED STATES GOVERNMENT, AS REPRESENTED BY GOVERNMENT AGENCY, IS AN
 * INTENDED THIRD-PARTY BENEFICIARY OF ALL SUBSEQUENT DISTRIBUTIONS OR
 * REDISTRIBUTIONS OF THE SUBJECT SOFTWARE. ANYONE WHO USES, REPRODUCES,
 * DISTRIBUTES, MODIFIES OR REDISTRIBUTES THE SUBJECT SOFTWARE, AS DEFINED
 * HEREIN, OR ANY PART THEREOF, IS, BY THAT ACTION, ACCEPTING IN FULL THE
 * RESPONSIBILITIES AND OBLIGATIONS CONTAINED IN THIS AGREEMENT.
 *
 * Government Agency: DHA: Web and Mobile Technology Program Management Office
 * Government Agency Original Software Designation: pwa-starter
 * Government Agency Original Software Title: pwa-starter
 * User Registration Requested. Please send email
 * with your contact information to: robert.a.kayl.civ@mail.mil
 * Government Agency Point of Contact for Original Software - Program Manager: robert.a.kayl.civ@mail.mil
 */

/*************** Class Component *********************

import { Button } from '@material-ui/core';
import { Tracker } from 'dha-analytics';
import React from 'react';

class Home extends React.Component {
  private path = window.location.hash;
  private tracker = Tracker();

  componentDidMount(): void {
    this.tracker({ dh: 'example.com', dp: this.path, dt: 'App', t: 'pageview' });
  }

  handleClick = (event: React.ChangeEvent<{}>) => {
    this.tracker({
      dp: this.path,
      ea: 'handleClick',
      ec: 'click',
      el: 'clickButton',
      ev: '1',
      t: 'event',
    });
  };

  render() {
    return (
      <Button color="primary" onClick={this.handleClick} variant="contained">
        Click
      </Button>
    );
  }
}

export default Home;
*/

/**************Funtional Component******************************************/

import { Button } from '@material-ui/core';
import { Tracker } from 'dha-analytics';
import React, { useEffect } from 'react';

const Home: React.FC = () => {
  const path = window.location.hash;
  const tracker = Tracker();

  useEffect(() => {
    tracker({ dh: 'example.com', dp: path, dt: 'App', t: 'pageview' });
  });

  const handleClick = (event: React.ChangeEvent<{}>) => {
    tracker({
      dp: path,
      ea: 'handleClick',
      ec: 'click',
      el: 'clickButton',
      ev: '1',
      t: 'event',
    });
  };

  return (
    <Button color="primary" onClick={handleClick} variant="contained">
      Click
    </Button>
  );
};

export default Home;
